package ru.og_dev.og_dev_ru;

public interface Constants {
    public final String DOMAIN = "og-dev.ru";
    public final String PROTOCOL = "https";
    public final String EMAIL_FROM = "site@og-dev.ru";
    public final String EMAIL_TO = "oleg.goborov@yandex.ru";

    public final String CONTACT_FORM_SUBJECT = "Новое сообщение из контактной формы";
    public final String CONTACT_FORM_SUCCESS = "Ваше сообение отправлено!";
}
