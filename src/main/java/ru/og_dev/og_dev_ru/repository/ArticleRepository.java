package ru.og_dev.og_dev_ru.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.og_dev.og_dev_ru.model.Article;

import java.util.Optional;

@Repository
public interface ArticleRepository extends CrudRepository<Article, Integer> {

    Optional<Article> findById(Long id);

    @Query(
        value = "SELECT * FROM articles a WHERE a.code= ?1 LIMIT 1",
        nativeQuery = true
    )
    Iterable<Article> findByCode(String code);

    @Query(
        value = "SELECT * FROM articles u " +
            "WHERE u.active = true AND u.lang = 'ru' " +
            "ORDER BY u.id DESC " +
            "LIMIT ?1 OFFSET ?2",
        nativeQuery = true
    )
    Iterable<Article> findAllByOrderByIdDesc(Integer cnt, Integer page);

    @Query(
        value = "SELECT COUNT(*) FROM articles a " +
            "WHERE a.active = true AND a.lang = 'ru'",
        nativeQuery = true
    )
    Integer getCnt();


}
