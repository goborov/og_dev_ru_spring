package ru.og_dev.og_dev_ru.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.og_dev.og_dev_ru.model.Provider;

@Repository
public interface ProviderRepository extends CrudRepository<Provider, Integer> {

    public Iterable<Provider> findAllByOrderByNameAsc();

}
