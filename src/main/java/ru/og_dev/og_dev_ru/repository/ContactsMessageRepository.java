package ru.og_dev.og_dev_ru.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.og_dev.og_dev_ru.model.ContactsMessage;

@Repository
public interface ContactsMessageRepository extends CrudRepository<ContactsMessage, Integer> {
}
