package ru.og_dev.og_dev_ru.repository;

import ru.og_dev.og_dev_ru.model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer> {
  public Iterable<Customer> findAllByOrderByCodeAsc();
}
