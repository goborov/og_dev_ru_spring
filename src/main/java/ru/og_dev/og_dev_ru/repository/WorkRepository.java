package ru.og_dev.og_dev_ru.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.og_dev.og_dev_ru.model.Work;

@Repository
public interface WorkRepository extends CrudRepository<Work, Long> {

    Work findById(Integer id);

    @Query(value = "SELECT * FROM works w " +
        "WHERE w.active = true " +
        "AND w.customer LIKE ?1 " +
        "AND (w.project LIKE ?2 OR w.project is NULL) " +
        "AND w.executor LIKE ?3 " +
        "AND w.status LIKE ?4 " +
        "AND w.date >= TO_TIMESTAMP(?5, 'YYYY-MM-DD HH24:MI:SS') " +
        "AND w.date <= TO_TIMESTAMP(?6, 'YYYY-MM-DD HH24:MI:SS') " +
        "ORDER BY w.date DESC, w.sort DESC, w.id DESC ",
        nativeQuery = true
    )
    Iterable<Work> findAllActiveOrderByIdAscSortDesc(String customer, String project, String executor,
                                                     String status, String from, String to);

    @Query(
        value = "SELECT * FROM works w " +
            "WHERE w.active = true " +
            "AND date = current_date ",
        nativeQuery = true
    )
    Iterable<Work> findTodayActive();

}
