package ru.og_dev.og_dev_ru.repository;

import ru.og_dev.og_dev_ru.model.Status;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRepository extends CrudRepository<Status, Integer> {
  public Iterable<Status> findAllByOrderByNameAsc();

}
