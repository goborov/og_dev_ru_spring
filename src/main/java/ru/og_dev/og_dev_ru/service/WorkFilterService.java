package ru.og_dev.og_dev_ru.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.og_dev.og_dev_ru.model.Customer;
import ru.og_dev.og_dev_ru.model.Project;
import ru.og_dev.og_dev_ru.model.Status;
import ru.og_dev.og_dev_ru.model.User;
import ru.og_dev.og_dev_ru.repository.CustomerRepository;
import ru.og_dev.og_dev_ru.repository.ProjectRepository;
import ru.og_dev.og_dev_ru.repository.StatusRepository;
import ru.og_dev.og_dev_ru.repository.UserRepository;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Map;

@Service
public class WorkFilterService {

    public String customer;
    public String project;
    public String executor;
    public String status;
    public String from;
    public String to;
    public String rst;

    @Autowired
    CustomerRepository customerRepository;
    public Iterable<Customer> customers;
    @Autowired
    ProjectRepository projectRepository;
    public Iterable<Project> projects;
    @Autowired
    UserRepository userRepository;
    @Autowired
    StatusRepository statusRepository;
    public Iterable<User> executors;
    public Iterable<Status> statuses;

    public Map<String, String> params;
    public HttpServletRequest req;
    public HttpServletResponse resp;

    final String COOKIE_PREFIX = "workFilter_";

    public WorkFilterService prepare(Map<String, String> getParams,
                                     HttpServletRequest request, HttpServletResponse response) {
        params = getParams;
        req = request;
        resp = response;

        rst = params.get("reset_filter");
        if (rst != null) resetFilter();

        setCustomer();
        setProject();
        setExecutor();
        setStatus();
        setFrom();
        setTo();

        getCostumers();
        getProjects();
        getExecutors();
        getStatuses();


        return this;
    }

    private void resetFilter() {
        clearCookie(resp, "customer");
        clearCookie(resp, "from");
        clearCookie(resp, "to");
    }

    private void setCustomer() {
        if (params.get("customer") != null) {
            customer = params.get("customer");
            setCookie(resp, "customer", customer);
        } else if (rst == null && !getCookie(req, "customer").equals(""))
            customer = getCookie(req, "customer");
        else customer = "%";
    }

    private void setProject() {
        if (params.get("project") != null) {
            project = params.get("project");
            setCookie(resp, "project", project);
        } else if (rst == null && !getCookie(req, "project").equals(""))
            project = getCookie(req, "project");
        else project = "%";
    }

    private void setExecutor() {
        if (params.get("executor") != null) {
            executor = params.get("executor");
            setCookie(resp, "executor", executor);
        } else if (rst == null && !getCookie(req, "executor").equals(""))
            executor = getCookie(req, "executor");
        else executor = "%";
    }

    private void setStatus() {
        if (params.get("status") != null) {
            status = params.get("status");
            setCookie(resp, "status", status);
        } else if (rst == null && !getCookie(req, "status").equals(""))
            status = getCookie(req, "status");
        else status = "%";
    }

    private void setFrom() {
        if (params.get("from") != null) {
            from = params.get("from");
            setCookie(resp, "from", from);
        } else if (rst == null && !getCookie(req, "from").equals(""))
            from = getCookie(req, "from");
        else from = LocalDate.now().minusMonths(3).toString();
    }

    private void setTo() {
        if (params.get("to") != null) {
            to = params.get("to");
            setCookie(resp, "to", to);
        } else if (rst == null && !getCookie(req, "to").equals(""))
            to = getCookie(req, "to");
        else to = LocalDate.now().plusMonths(1).toString();
    }

    private void clearCookie(HttpServletResponse resp, String name) {
        Cookie cookie = new Cookie("workFilter_" + name, "");
        cookie.setPath("/");
        resp.addCookie(cookie);
    }

    private String getCookie(HttpServletRequest req, String name) {
        return Arrays.stream(req.getCookies())
                .filter(c -> c.getName().equals("workFilter_" + name))
                .findFirst()
                .map(Cookie::getValue)
                .orElse("");
    }

    private void setCookie(HttpServletResponse resp, String name, String value) {
        Cookie cookie = new Cookie("workFilter_" + name, value);
        cookie.setPath("/");
        resp.addCookie(cookie);
    }

    private void getCostumers() {
        customers = customerRepository.findAllByOrderByCodeAsc();
    }

    private void getProjects() {
        projects = projectRepository.findAll();
    }

    private void getExecutors() {
        executors = userRepository.getExecutors();
    }

    private void getStatuses() {
        statuses = statusRepository.findAllByOrderByNameAsc();
    }

}
