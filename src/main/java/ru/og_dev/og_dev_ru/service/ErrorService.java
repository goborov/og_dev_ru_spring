package ru.og_dev.og_dev_ru.service;

import org.springframework.stereotype.Service;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Service
public class ErrorService {
    public String error;
    public String title, h1, desc;
    public HashMap<String, String> crumbs = new HashMap<>();

    public ErrorService show(HttpServletRequest request) {

        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        if (status != null) error = status.toString();

        h1 = "Ошибка";
        title = h1;
        desc = title;
        crumbs.put("#", h1);

        return this;
    }

}
