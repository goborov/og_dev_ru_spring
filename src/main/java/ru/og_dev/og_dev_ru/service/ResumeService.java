package ru.og_dev.og_dev_ru.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class ResumeService {

    public String title, h1, desc;
    public HashMap<String, String> crumbs = new HashMap<>();

    public ResumeService show() {

        h1 = "Резюме Middle Back-End разработчик PHP /Junior Java";
        title = h1;
        desc = title;
        crumbs.put("#", "Резюме");

        return this;
    }
}
