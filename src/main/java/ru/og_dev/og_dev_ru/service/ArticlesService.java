package ru.og_dev.og_dev_ru.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.og_dev.og_dev_ru.model.Article;
import ru.og_dev.og_dev_ru.repository.ArticleRepository;

import java.util.HashMap;

@Service
public class ArticlesService {

    public Iterable<Article> articles;
    private int cnt;
    private int pages;
    private int curPage;
    final private static int ON_PAGE = 10;
    public HashMap<Integer, Integer> pagen;

    public String title;
    public String h1;
    public String desc;
    public HashMap<String, String> crumbs;

    public ArticleRepository articleRepository;

    @Autowired
    public ArticlesService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public ArticlesService getForPageList(int page) {
        curPage = (page != 0 ? page : 1);
        articles = articleRepository.findAllByOrderByIdDesc(
            ON_PAGE, (curPage - 1) * ON_PAGE);
        getArticleCnt();
        getPagesCnt();
        createPagination();
        h1 = "Записки";
        title = "Записки, шпаргалки";
        desc = title;
        crumbs = new HashMap<>();
        crumbs.put("#", h1);
        return this;
    }

    private void getArticleCnt() {
        cnt = articleRepository.getCnt();
    }

    private void getPagesCnt() {
        pages = (int) Math.ceil((double) cnt / ON_PAGE);
    }

    private void createPagination() {
        pagen = new HashMap<>();
        for (int i = 1; i <= pages; i++) {
            if (i == curPage) {
                pagen.put(i, 1);
            } else {
                pagen.put(i, 0);
            }
        }
    }

}
