package ru.og_dev.og_dev_ru.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ru.og_dev.og_dev_ru.Constants;
import ru.og_dev.og_dev_ru.model.ContactsMessage;
import ru.og_dev.og_dev_ru.repository.ContactsMessageRepository;

import java.util.HashMap;

@Service
public class ContactsService {

    @Autowired
    private JavaMailSender javaMailSender;

    public String title, h1, desc;
    public HashMap<String, String> crumbs = new HashMap<>();

    @Autowired
    private ContactsMessageRepository contactsMessageRepository;
    public String msg;

    public ContactsService show() {
        h1 = "Контакты";
        title = h1;
        desc = title;
        crumbs.put("#", h1);

        return this;
    }

    public ContactsService sendMessage(ContactsMessage contactsMessage) {

        if (!contactsMessage.getEmail().isEmpty()) {

            contactsMessageRepository.save(contactsMessage);

            SimpleMailMessage msg = new SimpleMailMessage();
            msg.setFrom(Constants.EMAIL_FROM);
            msg.setTo(Constants.EMAIL_TO);
            msg.setSubject(Constants.CONTACT_FORM_SUBJECT);
            msg.setText(
                    "Имя: " + contactsMessage.getName() + " \n" +
                            "Эл.почта: " + contactsMessage.getEmail() + " \n" +
                            "Телефон: " + contactsMessage.getPhone() + " \n\n" +
                            "Сообщение: \n" + contactsMessage.getMsg() + "\n"
            );

            javaMailSender.send(msg);

        }

        this.msg = Constants.CONTACT_FORM_SUCCESS;
        return this;
    }
}
