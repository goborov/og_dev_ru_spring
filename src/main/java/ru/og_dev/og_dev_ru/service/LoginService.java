package ru.og_dev.og_dev_ru.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class LoginService {
    public String title, h1, desc;
    public HashMap<String, String> crumbs = new HashMap<>();

    public LoginService show() {
        h1 = "Вход";
        title = h1;
        desc = title;
        crumbs.put("#", h1);

        return this;
    }
}
