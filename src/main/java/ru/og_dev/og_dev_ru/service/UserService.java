package ru.og_dev.og_dev_ru.service;

import ru.og_dev.og_dev_ru.model.User;
import ru.og_dev.og_dev_ru.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired(required = false)
    private UserRepository userRepository;

    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    public long count() {
        return userRepository.count();
    }

    public User findById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Autowired(required = false)
    public Iterable<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    public User getCurUser() {
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        if (loggedInUser == null) return null;
        String username = loggedInUser.getName();
        if (username.equals("anonymousUser")) return null;
        return findByEmail(username).iterator().next();
    }


}
