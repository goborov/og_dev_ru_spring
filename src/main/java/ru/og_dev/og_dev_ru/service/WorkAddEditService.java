package ru.og_dev.og_dev_ru.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.og_dev.og_dev_ru.model.*;
import ru.og_dev.og_dev_ru.repository.*;

import java.util.Date;
import java.util.HashMap;

@Service
public class WorkAddEditService {

    public Work work;
    public String mode;
    public Iterable<Project> projects;
    public Iterable<Customer> customers;
    public Iterable<Provider> providers;
    public Iterable<Status> statuses;
    public Iterable<User> executors;

    @Autowired
    WorkRepository workRepository;
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    ProviderRepository providerRepository;
    @Autowired
    StatusRepository statusRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserService userService;

    public String title, h1, desc;
    public HashMap<String, String> crumbs = new HashMap<>();

    private void initFields() {
        projects = projectRepository.findAllByOrderByCodeAsc();
        customers = customerRepository.findAllByOrderByCodeAsc();
        providers = providerRepository.findAllByOrderByNameAsc();
        statuses = statusRepository.findAllByOrderByNameAsc();
        executors = userRepository.getExecutors();
    }

    public WorkAddEditService getData(Long id) {
        work = workRepository.findById(id).get();
        mode = "edit";
        initFields();

        h1 = "Редактировние записи #" + work.getId();
        title = h1;
        desc = title;
        crumbs.put("#", title);

        return this;
    }

    public WorkAddEditService setData(Work workData, String delete) {
        Work work = workRepository.findById(workData.getId()).get();
        if (delete.equals("y")) work.setActive(false);
        work.setName(workData.getName());
        work.setTask(workData.getTask());
        work.setPrediction(workData.getPrediction());
        work.setFact(workData.getFact());
        work.setDate(workData.getDate());
        work.setCustomer(workData.getCustomer());

        if (workData.getProject().equals("")) work.setProject(null);
        else work.setProject(workData.getProject());

        if (workData.getProvider().equals("")) work.setProvider(null);
        else work.setProvider(workData.getProvider());

        work.setExecutor(workData.getExecutor());
        work.setStatus(workData.getStatus());
        work.setExt_id(workData.getExt_id());
        work.setSort(workData.getSort());
        work.setPriority(workData.getPriority());
        workRepository.save(work);
        return this;
    }

    public WorkAddEditService addPage() {
        mode = "add";
        work = new Work();
        if (work.getDate() == null) work.setDate(new Date());
        initFields();

        User user = userService.getCurUser();
        if (user.getInitials() != null) this.work.setExecutor(user.getInitials());

        h1 = "Добавление записи";
        title = h1;
        desc = title;
        crumbs.put("#", title);

        return this;
    }

    public WorkAddEditService addData(Work work) {
        work.setActive(true);
        work.setSort(0);
        work.setPriority((byte) 0);
        workRepository.save(work);
        return this;
    }

}
