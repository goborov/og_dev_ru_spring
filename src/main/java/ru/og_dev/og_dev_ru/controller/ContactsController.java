package ru.og_dev.og_dev_ru.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.og_dev.og_dev_ru.model.ContactsMessage;
import ru.og_dev.og_dev_ru.service.ContactsService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class ContactsController extends BasicController {

    @Autowired
    private ContactsService contactsService;

    @GetMapping("/contacts")
    public String showContacts(Model model, HttpServletRequest request) throws IOException {
        ContactsService ob = contactsService.show();
        model.addAttribute("ob", ob);
        setTheme(model, request);
        return "contacts";
    }

    @PostMapping("/contacts/send")
    public void sendMsg(@ModelAttribute ContactsMessage contactsMessage, Model model,
                        HttpServletRequest request, HttpServletResponse httpServletResponse) throws IOException {
        ContactsService ob = contactsService.sendMessage(contactsMessage);
        model.addAttribute("ob", ob);
        httpServletResponse.sendRedirect("/contacts");
    }

}
