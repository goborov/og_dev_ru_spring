package ru.og_dev.og_dev_ru.controller;

import ru.og_dev.og_dev_ru.service.SitemapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SitemapController {

    @Autowired
    SitemapService sitemapService;

    @GetMapping("/sitemap.xml")
    @ResponseBody
    public String showMap() {
        return sitemapService.getSiteMap();
    }
}
