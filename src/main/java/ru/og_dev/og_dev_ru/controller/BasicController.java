package ru.og_dev.og_dev_ru.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

@Controller
public class BasicController {
  private String theme;

  public void setTheme(Model model, HttpServletRequest request) {
    Cookie cookie = WebUtils.getCookie(request, "theme");
    if (cookie == null) theme = "light";
    else theme = cookie.getValue();
    model.addAttribute("theme", theme);
  }

}
