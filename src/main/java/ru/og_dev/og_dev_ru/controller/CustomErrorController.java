package ru.og_dev.og_dev_ru.controller;

import ru.og_dev.og_dev_ru.service.ErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class CustomErrorController extends BasicController implements ErrorController {

    @Autowired
    private ErrorService errorService;

    @RequestMapping("/error")
    public String handleError(Model model, HttpServletRequest request) {

        ErrorService ob = errorService.show(request);
        model.addAttribute("ob", ob);
        setTheme(model, request);
        return "error";
    }
}
