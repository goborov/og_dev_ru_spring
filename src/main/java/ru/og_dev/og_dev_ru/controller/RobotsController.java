package ru.og_dev.og_dev_ru.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.og_dev.og_dev_ru.Constants;

@Controller
public class RobotsController implements Constants {
    @GetMapping("/robots.txt")
    @ResponseBody
    public String showRobots() {
        String robots = "User-agent: *" +
            "\nDisallow:" +
            "\nHost: " + PROTOCOL + "://" + DOMAIN +
            "\nSitemap: " + PROTOCOL + "://" + DOMAIN + "/sitemap.xml";
        return robots;
    }
}
