package ru.og_dev.og_dev_ru.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.og_dev.og_dev_ru.model.Work;
import ru.og_dev.og_dev_ru.service.WorkAddEditService;
import ru.og_dev.og_dev_ru.service.WorkFilterService;
import ru.og_dev.og_dev_ru.service.WorkService;
import ru.og_dev.og_dev_ru.service.WorkStatisticService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Controller
public class WorkController extends BasicController {

    @Autowired
    private WorkService workService;

    @Autowired
    private WorkFilterService workFilterService;

    @Autowired
    private WorkStatisticService workStatisticService;

    @Autowired
    private WorkAddEditService workAddEditService;

    @GetMapping("/my/works")
    public String getWorks(@RequestParam Map<String, String> getParams,
                           Model model, HttpServletRequest request, HttpServletResponse response) {
        WorkFilterService filter = workFilterService.prepare(getParams, request, response);
        WorkService ob = workService.getForListPage(filter);
        WorkStatisticService stat = workStatisticService.getToday();
        model.addAttribute("ob", ob);
        model.addAttribute("filter", filter);
        model.addAttribute("stat", stat);
        setTheme(model, request);
        return "my/works";
    }

    @GetMapping("/my/works.csv")
    public ResponseEntity getWorksCsv(@RequestParam Map<String, String> getParams, Model model,
                                      HttpServletRequest request, HttpServletResponse response) throws IOException {
        WorkFilterService filter = workFilterService.prepare(getParams, request, response);
        WorkService ob = workService.getForListPage(filter);
        String csv = workService.getCSV(ob);


        /*response.setContentType("text/plain; charset=utf-8");
        response.getWriter().print(csv);*/

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "application/vnd.ms-excel");
        responseHeaders.add("Content-Disposition", "attachment; filename=works.csv");
        return new ResponseEntity<>(csv.getBytes("UTF-8"), responseHeaders, HttpStatus.OK);

    }

    @GetMapping("/my/work/edit/{id}")
    public String showWork(@PathVariable Long id, Model model, HttpServletRequest request) {
        WorkAddEditService ob = workAddEditService.getData(id);
        model.addAttribute("ob", ob);
        model.addAttribute("title", "Задача #" + ob.work.getId());
        setTheme(model, request);
        return "my/add_edit";
    }

    @PostMapping("/my/work/edit")
    public void updateWork(@ModelAttribute Work work, @RequestParam(defaultValue = "n") String delete,
                           HttpServletResponse httpServletResponse) throws IOException {
        WorkAddEditService ob = workAddEditService.setData(work, delete);
        httpServletResponse.sendRedirect("/my/works");
    }

    @GetMapping("/my/work/add")
    public String addWorkPage(Model model, HttpServletRequest request) {
        WorkAddEditService ob = workAddEditService.addPage();
        model.addAttribute("ob", ob);
        model.addAttribute("title", "Добавление задачи");
        setTheme(model, request);
        return "my/add_edit";
    }

    @PostMapping("/my/work/add")
    public void addWork(@ModelAttribute Work work,
                        HttpServletResponse httpServletResponse) throws IOException {
        WorkAddEditService ob = workAddEditService.addData(work);
        httpServletResponse.sendRedirect("/my/works");
    }

}
