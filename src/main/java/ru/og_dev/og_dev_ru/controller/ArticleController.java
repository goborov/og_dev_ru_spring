package ru.og_dev.og_dev_ru.controller;

import ru.og_dev.og_dev_ru.model.Article;
import ru.og_dev.og_dev_ru.service.ArticleService;
import ru.og_dev.og_dev_ru.service.ArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class ArticleController extends BasicController {

    @Autowired
    private ArticlesService articlesService;
    @Autowired
    private ArticleService articleService;

    @GetMapping("/articles")
    public String showArticles(@RequestParam(defaultValue = "1") Integer page,
                               Model model, HttpServletRequest request) {
        ArticlesService ob = articlesService.getForPageList(page);
        model.addAttribute("ob", ob);
        setTheme(model, request);
        return "articles";
    }

    @GetMapping("/articles/{code}")
    public String showArticle(@PathVariable("code") String code,
                              Model model, HttpServletRequest request) {
        ArticleService ob = articleService.get(code);
        model.addAttribute("ob", ob);
        setTheme(model, request);
        return "article";
    }

    @GetMapping("/article/edit/{id}")
    public String editPageArticle(@PathVariable("id") Long id,
                                  Model model, HttpServletRequest request) {
        ArticleService ob = articleService.editPage(id);
        model.addAttribute("ob", ob);
        setTheme(model, request);
        return "article_edit";
    }

    @PostMapping("/article/save")
    public void saveArticle(@ModelAttribute Article article, Model model, HttpServletRequest request,
                            HttpServletResponse httpServletResponse) throws IOException {
        String code = articleService.save(article);
        httpServletResponse.sendRedirect("/articles/" + code);
    }

}
