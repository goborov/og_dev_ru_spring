package ru.og_dev.og_dev_ru.controller;

import ru.og_dev.og_dev_ru.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginController extends BasicController {

    @Autowired
    LoginService loginService;

    @GetMapping("/login")
    public String get(Model model, HttpServletRequest request) {
        //model.addAttribute("title", "Форма входа");

        LoginService ob = loginService.show();
        model.addAttribute("ob", ob);

        setTheme(model, request);
        return "login";
    }
}
