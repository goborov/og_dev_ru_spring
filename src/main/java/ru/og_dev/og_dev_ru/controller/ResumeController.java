package ru.og_dev.og_dev_ru.controller;

import ru.og_dev.og_dev_ru.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
public class ResumeController extends BasicController {
    @Autowired
    private ResumeService resumeService;

    @GetMapping("/resume")
    public String showResume(Model model, HttpServletRequest request) throws IOException {
        ResumeService ob = resumeService.show();
        model.addAttribute("ob", ob);
        setTheme(model, request);
        return "resume";
    }
}
