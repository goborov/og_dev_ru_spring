package ru.og_dev.og_dev_ru.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import ru.og_dev.og_dev_ru.providers.CustomAuthenticationProvider;

@Configuration
@EnableWebSecurity
public class SecSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomAuthenticationProvider customAuthenticationProvider;

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .authenticationProvider(customAuthenticationProvider)
            .authorizeRequests()
            .antMatchers("/contacts/send").permitAll()
            .antMatchers("/my**").authenticated() // auth only
            .antMatchers("/login*").permitAll()
            .antMatchers("/*").permitAll()
//            .antMatchers("/list").permitAll()
//            .antMatchers("/all*").permitAll()
            .antMatchers("/articles/*").permitAll()
            .anyRequest().authenticated()
            .and()
            .formLogin().permitAll()
            .loginPage("/login")
            .defaultSuccessUrl("/my")
            .and()
            .logout().permitAll();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(new CustomAuthenticationProvider());
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/img/**", "/css/**", "/js/**");
    }

}
