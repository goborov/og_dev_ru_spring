package ru.og_dev.og_dev_ru.providers;

import ru.og_dev.og_dev_ru.model.User;
import ru.og_dev.og_dev_ru.repository.UserRepository;
import ru.og_dev.og_dev_ru.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

  @Autowired
  private UserRepository userRepository;
  private UserService userService;

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    String email = authentication.getName();
    String password = authentication.getCredentials().toString();

    User user;
    try {
      user = userRepository.findByEmail(email).iterator().next();
      if (!email.equals(user.getEmail()) || !password.equals(user.getPasswordPlain()))
        throw new BadCredentialsException("Неверная почта или пароль.");
    } catch (Exception e) {
      throw new BadCredentialsException("Нет такого пользователя.");
    }

    List<GrantedAuthority> authorities = new ArrayList<>();
    authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getPermission()));

    Authentication auth = new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPasswordPlain(), authorities);
    return auth;
  }

  @Override
  public boolean supports(Class<?> aClass) {
    return aClass.equals(UsernamePasswordAuthenticationToken.class);
  }
}