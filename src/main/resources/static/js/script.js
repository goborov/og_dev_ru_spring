$(function () {

  ///////////////////////////////////////////////////////////// page articles
  $('.page-articles input').on('change', function () {
    $(this).closest('form').submit();
  });


  document.querySelectorAll('pre code').forEach((el) => {
    hljs.highlightElement(el);
  });

  // translit
  $('.page-article form [name="name"]').on('keyup', function () {
    val = transliterate($(this).val());
    val = val.replaceAll(" ", "-");
    val = val.replaceAll(",", "");
    val = val.replaceAll('"', "");
    val = val.replaceAll("'", "");
    val = val.replaceAll(".", "");
    val = val.replaceAll("/", "-");
    val = val.toLowerCase();
    $('.page-article form [name="code"]').val(val);
  });

    // header theme switcher
    /*theme = getCookie("theme");
    if(theme == null) theme = 'light';
    $('<link rel="stylesheet" href="/css/' + theme + '.css" type="text/css" />').appendTo('head');*/

    $('header .theme-selector span').on('click', function () {
        theme = $(this).data('theme');
        setCookie('theme', theme, 1000);
        window.location.reload();
    });

    // copy task from list
    $('.page-my-works .js-copy').on('click', function (e) {
        e.preventDefault();

        tr = $(this).closest('tr');

        copy = "og_" + $(tr).find('.id').text() + ";" +
                        $(tr).find('.ext_id').text() + ";" +
                        $(tr).find('.name').text();
        console.log(copy);
        copyToClipboard(copy);

    });



    // copy to clipboard
    function copyToClipboard(text) {
        if (window.clipboardData && window.clipboardData.setData)
            return window.clipboardData.setData("Text", text);
        else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
            var textarea = document.createElement("textarea");
            textarea.textContent = text;
            textarea.style.position = "fixed";
            document.body.appendChild(textarea);
            textarea.select();
            try {
                return document.execCommand("copy");
            }
            catch (ex) {
                console.warn("Copy to clipboard failed.", ex);
                return false;
            }
            finally {
                document.body.removeChild(textarea);
            }
        }
    }


});
